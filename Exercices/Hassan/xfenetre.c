#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define largeur_fenetre 200
#define hauteur_fenetre 200
#define nb_fenetre 40
SDL_DisplayMode screen;

int main(int argc, char *argv[]) {
    (void)argc;
    (void)argv;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Erreur  de l'initialisation de SDL: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    if (SDL_GetCurrentDisplayMode(0, &screen) != 0) {
        SDL_Log("Erreur  de la récupération du mode d'affichage: %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Window **fenetre = (SDL_Window **)calloc(nb_fenetre, sizeof(SDL_Window *));
    if (fenetre == NULL) {
        SDL_Log("Erreur  de l'allocation de mémoire pour les fenêtres.\n");
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < nb_fenetre; ++i) {
        int x = 0, y = 0;
        if (i != 0) {
            SDL_GetWindowPosition(fenetre[i - 1], &x, &y);
        }

        fenetre[i] = SDL_CreateWindow("fenetre", largeur_fenetre ,
                                      hauteur_fenetre ,
                                       largeur_fenetre ,
                                         hauteur_fenetre , 0);
        if (fenetre[i] == NULL) {
            SDL_Log("Erreur  de la création de la fenêtre: %s\n", SDL_GetError());
            for (int j = 0; j < i; ++j) {
                SDL_DestroyWindow(fenetre[j]);
            }
            free(fenetre);
            SDL_Quit();
            exit(EXIT_FAILURE);
        }

        SDL_SetWindowPosition(fenetre[i], x+20, y+5);
    }

    int max_hauteur = screen.h - hauteur_fenetre;
    int max_largeur = screen.w - largeur_fenetre;
    bool bouge = true;
    while (bouge) {
        bouge = false;
        for (int i = 0; i < nb_fenetre; ++i) {
            int x, y;
            SDL_GetWindowPosition(fenetre[i], &x, &y);
            y +=10;
            x+=10;


            if (y < max_hauteur || x < max_largeur) {
                SDL_SetWindowPosition(fenetre[i], x, y);
                bouge = true;
            }
        }
        SDL_Delay(3000); 
    }

    for (int i = 0; i < nb_fenetre; ++i) {
        if (fenetre[i] != NULL) {
            SDL_DestroyWindow(fenetre[i]);
        }
    }
    free(fenetre);
    SDL_Quit();
    return 0;
}
