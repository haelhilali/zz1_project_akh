#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

typedef struct carre_t{
    int posisition_x;           
    int posisition_y;           
    int largeur;         
    int hauteur; 
    int rayon;
    float angle;
    int centre_mvmt_x ;
    int centre_mvmt_y;        
} carre_t;

#define vitesse_rotation 1


void end_sdl(char ok,                                               
             char const* msg,                                       
             SDL_Window* window,                                   
             SDL_Renderer* renderer) {                              
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           
    SDL_DestroyRenderer(renderer);                                  
    renderer = NULL;
  }
  if (window != NULL)   {                                           
    SDL_DestroyWindow(window);                                     
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                                   
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}




void fond(SDL_Renderer * renderer, int w, int h){
    SDL_Rect rectangle;                                                             

    SDL_SetRenderDrawColor(renderer,                                                
                            255, 255, 255,                                  
                            255);                                     
    rectangle.x = 0;                                                 
    rectangle.y = 0;                                                  
    rectangle.w = w;                                                
    rectangle.h = h;                                               

    SDL_RenderFillRect(renderer, &rectangle);            
}






void draw(SDL_Renderer* renderer, int posisition_x, int posisition_y, int largeur, int hauteur) {                              

  SDL_Rect rectangle;                                                             

    SDL_SetRenderDrawColor(renderer,                                                
                            255, 0, hauteur,                                  
                            130);                                    
    rectangle.x = posisition_x;                                                 
    rectangle.y = posisition_y;                                            
    rectangle.w = largeur;                                          
    rectangle.h = hauteur;                                            

    SDL_RenderFillRect(renderer, &rectangle);                                  

}



int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    int wind_w, wind_h;
    int nb_carre = 5;
    carre_t tab[nb_carre];
    tab[0].largeur = 50, tab[0].angle=0, tab[0].posisition_x = 300, tab[0].posisition_y=400, tab[0].rayon=100, tab[0].hauteur=50, tab[0].centre_mvmt_x=300,tab[0].centre_mvmt_y=333;
    tab[1].largeur = 34, tab[1].angle=0, tab[1].posisition_x = 400, tab[1].posisition_y=600, tab[1].rayon=200, tab[1].hauteur=20, tab[1].centre_mvmt_x=400,tab[1].centre_mvmt_y=444;
    tab[2].largeur = 12, tab[2].angle=0, tab[2].posisition_x = 150, tab[2].posisition_y=300, tab[2].rayon=150, tab[2].hauteur=44, tab[2].centre_mvmt_x=222,tab[2].centre_mvmt_y=222;
    tab[3].largeur = 23, tab[3].angle=0, tab[3].posisition_x = 500, tab[3].posisition_y=300, tab[3].rayon=300, tab[3].hauteur=25, tab[3].centre_mvmt_x=222,tab[3].centre_mvmt_y=333;
    tab[4].largeur = 66, tab[4].angle=0, tab[4].posisition_x = 400, tab[4].posisition_y=200, tab[4].rayon=250, tab[4].hauteur=33, tab[4].centre_mvmt_x=200,tab[4].centre_mvmt_y=150;
      

    SDL_Event event;
    SDL_bool prg_on = SDL_TRUE;

    

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    SDL_DisplayMode screen;


    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
            screen.w, screen.h);

    window = SDL_CreateWindow("fenetre",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, screen.w * 0.7,
                                screen.h * 0.7,
                                SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    wind_w = screen.w*0.7;
    wind_h = screen.h*0.7;

    renderer = SDL_CreateRenderer(window, -1,
                                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    while(prg_on){

        while(SDL_PollEvent(&event)){                     
                                                        
            switch(event.type){                        
            case SDL_QUIT:                              
                prg_on = SDL_FALSE;                     
                break;
            default:                                    
                break;
            }
        }

        fond(renderer, wind_w, wind_h);
      

        for(int i=0;i<2;i++){
            tab[i].posisition_x = tab[i].centre_mvmt_x+tab[i].rayon*7*cos(tab[i].angle*3.14/180.0);  
            tab[i].posisition_y = tab[i].centre_mvmt_y+tab[i].rayon; 
            draw(renderer, tab[i].posisition_x, tab[i].posisition_y, tab[i].largeur, tab[i].hauteur);
        }
        for(int i=2;i<5;i++){
            tab[i].posisition_x = tab[i].centre_mvmt_x+tab[i].rayon;
            tab[i].posisition_y = tab[i].centre_mvmt_y+tab[i].rayon*3*sin(tab[i].angle*3.14/180.0);  
            draw(renderer, tab[i].posisition_x, tab[i].posisition_y, tab[i].largeur, tab[i].hauteur);
        }
        SDL_RenderPresent(renderer);
        SDL_Delay(5); 
        for(int i = 0; i < 5; i++) {
            tab[i].angle += vitesse_rotation;  //  augmentation de l'angle pour  bouger les rectangles
        }
     
    }

    end_sdl(1, "fin", window, renderer);
    return EXIT_SUCCESS;
}