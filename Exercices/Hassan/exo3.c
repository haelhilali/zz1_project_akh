#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#define M_PI 3.14159265358979323846

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer) {
 char msg_formated[255];
 int l;

 if (!ok) {
 strncpy(msg_formated, msg, 250);
 l = strlen(msg_formated);
 strcpy(msg_formated + l, " : %s\n");
 SDL_Log(msg_formated, SDL_GetError());
 }

 if (renderer != NULL) {
 SDL_DestroyRenderer(renderer);
 renderer = NULL;
 }
 if (window != NULL) {
 SDL_DestroyWindow(window);
 window = NULL;
 }

 SDL_Quit();

 if (!ok) {
 exit(EXIT_FAILURE);
 }
}
void Draw_background_texture(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer) {
 SDL_Rect 
 source = {0}, 
 window_dimensions = {0},
 destination = {0}; 

 SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h); 
 SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h); 
 destination = window_dimensions;

 SDL_RenderCopy(renderer, my_texture, &source, &destination); 
}
 void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,
 SDL_Renderer *renderer) {
 SDL_Rect 
 source = {0}, 
 window_dimensions = {0}, 
 destination = {0}; 

 SDL_GetWindowSize(
 window, &window_dimensions.w,
 &window_dimensions.h); 
 SDL_QueryTexture(my_texture, NULL, NULL,
 &source.w, &source.h); 

 destination = window_dimensions; 


 SDL_RenderCopy(renderer, my_texture,
 &source,
 &destination); 
 SDL_RenderPresent(renderer); 
 SDL_Delay(2000); 

 SDL_RenderClear(renderer); 
 }

SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Window* window, SDL_Renderer* renderer) {
 SDL_Surface* my_image = IMG_Load(file_image_name);
 if (my_image == NULL) {
 end_sdl(0, "Unable to load image", window, renderer);
 }

 SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
 SDL_FreeSurface(my_image);
 if (my_texture == NULL) {
 end_sdl(0, "Failed to convert surface to texture", window, renderer);
 }

 return my_texture;
}

void play_with_texture_3(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer) {
 SDL_Rect source = {0}, window_dimensions = {0}, destination = {0};

 SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
 SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

 float zoom = 0.25;
 int nb_it = 200;
 destination.w = source.w * zoom;
 destination.h = source.h * zoom;
 destination.x = (window_dimensions.w - destination.w) / 2;
 float h = window_dimensions.h - destination.h;

 for (int i = 0; i < nb_it; ++i) {
 destination.y = h * (1 - exp(-5.0 * i / nb_it) / 2 * (1 + cos(10.0 * i / nb_it * 2 * M_PI)));

 SDL_RenderClear(renderer);

 SDL_SetTextureAlphaMod(my_texture, (1.0 - 1.0 * i / nb_it) * 255);
 SDL_RenderCopy(renderer, my_texture, &source, &destination);
 SDL_RenderPresent(renderer);
 SDL_Delay(30);
 }
 SDL_RenderClear(renderer);
}

void play_with_texture_4(SDL_Texture* my_texture1,SDL_Texture* my_texture2, SDL_Window* window, SDL_Renderer* renderer) {
 SDL_Rect source = {0}, window_dimensions = {0}, destination = {0}, state = {0};

 SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
 SDL_QueryTexture(my_texture1, NULL, NULL, &source.w, &source.h);

 int nb_images = 5;
 float zoom = 1;
 int offset_x = source.w / nb_images, offset_y = source.h / 8;

 state.x = 0;
 state.y = 3 * offset_y;
 state.w = offset_x ;
 state.h = offset_y ;

 destination.w = offset_x * zoom ;
 destination.h = offset_y * zoom ;
 destination.y = (window_dimensions.h - destination.h) / 2;

 int speed = 5;
 for (int x = 0; x < window_dimensions.w - destination.w; x += speed) {
 destination.x = x;
 state.x += offset_x;
 state.x %= source.w;

 SDL_RenderClear(renderer);
  Draw_background_texture(my_texture2, window, renderer);

 SDL_RenderCopy(renderer, my_texture1, &state, &destination);
 SDL_RenderPresent(renderer);
 SDL_Delay(80);
 }
 SDL_RenderClear(renderer);
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
 if (SDL_Init(SDL_INIT_VIDEO) != 0) {
 SDL_Log("Erreur d'initialisation - %s\n", SDL_GetError());
 exit(EXIT_FAILURE);
 }

 SDL_Window* window = SDL_CreateWindow("Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_RESIZABLE);
 if (window == NULL) {
 SDL_Log("Erreur de creation de fenetre: %s\n", SDL_GetError());
 SDL_Quit();
 exit(EXIT_FAILURE);
 }

 SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
 if (renderer == NULL) {
 end_sdl(0, "Erreur de creation du rendu ", window, renderer);
 }
 SDL_SetRenderDrawColor(renderer,255,255,255,255);

 if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) == 0) {
 end_sdl(0, "Erreur initialisation SDL_image", window, renderer);
 }
 SDL_Texture* texture0 = load_texture_from_image("//home/hassanelhilai/Desktop/all_exo/sun.png", window, renderer);
 play_with_texture_1(texture0, window, renderer); 

 SDL_Texture* texture1 = load_texture_from_image("//home/hassanelhilai/Desktop/all_exo/basketball_texture.png", window, renderer);
 play_with_texture_3(texture1, window, renderer);
 SDL_Delay(500);
 SDL_Texture* texture2 = load_texture_from_image("/home/hassanelhilai/Desktop/all_exo/spritesheet_50x50.png", window, renderer);
  SDL_Texture* texture3 = load_texture_from_image("/home/hassanelhilai/Desktop/all_exo/preview.png", window, renderer);
play_with_texture_4(texture2,texture3, window, renderer);


 SDL_DestroyTexture(texture1);
 SDL_DestroyTexture(texture2);

 end_sdl(1, "fin", window, renderer);

 return 0;
}