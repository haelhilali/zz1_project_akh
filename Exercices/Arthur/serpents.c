#include <SDL2/SDL.h>
#include "serpents.h"

#define DELAY 20
#define G 9.81f
#define NBSQUARES 500
#define SIZESQUARE 5
#define ABS(A) ( ((A) > 0) ? (A) : (-(A)) )

Point Point_init(int x, int y){
   Point a;
   a.x = x;
   a.y = y;
   return a;
}

Movement Movement_init(int vx , int x, int y){
   Movement m;
   m.p.x = x;
   m.p.x = y;
   m.vx = vx;
   return m;
}

void Show_object(Object obj){
   printf("%d %d ", obj.init_pos.x, obj.init_pos.y);
   printf("%d %d ", obj.m.p.x, obj.m.p.y);
   printf("%d\n\n", obj.m.vx);
}

Object Object_init(int vx, int x, int y, int size, int r, int g, int b){
   Object obj;
   obj.init_pos = Point_init(x, y);
   obj.m = Movement_init(vx, x, y);
   obj.c = (SDL_Color){.r = r, .b = b, .g = g, .a = 255};
   obj.size = size;
   return obj;
} 

void Object_init_random(Object *objs, int width, int height){
   for(int i=0; i<NBSQUARES; ++i){
      int vx = rand() % 30;
      int x = rand() % width;
      int y = rand() % height/3;
      int r = rand() % 255;
      int g = rand() % 255;
      int b = rand() % 255;
      int size = rand() % SIZESQUARE + 5;
      objs[i] = Object_init(vx, x, y, size, r, g, b);
   }
}

void Update_object(Object *obj, float t){
   obj->m.p.x = obj->init_pos.x + obj->m.vx * t;
   obj->m.p.y = obj->init_pos.y - 0.5f * G * t * t;
}

int Draw_object_dt(SDL_Renderer *renderer, Object *obj, int height){
   int y = obj->m.p.y;

   if(ABS(y) > height - 50) return 0;

   SDL_Rect rectangle = {.x = obj->m.p.x, .y = ABS(y), .w = obj->size, .h = obj->size};  

   SDL_SetRenderDrawColor(renderer, obj->c.r, obj->c.g, obj->c.b, obj->c.a);
   SDL_RenderFillRect(renderer, &rectangle);
   return 1;
}

void Squares_rain(SDL_Renderer *renderer, Object *objs, float t, int height){


   for(int i=0; i<NBSQUARES; ++i){
      int tmp = Draw_object_dt(renderer, &objs[i], height/1.5f);
      if(tmp)
         Update_object(&objs[i], t);
   }
}

int main(void) {
   srand(time(NULL));   
   SDL_Window *window = NULL;
   SDL_DisplayMode DM;

   if (SDL_Init(SDL_INIT_VIDEO) != 0) {
      SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }

   /* Taille de l'écran */
   if (SDL_GetCurrentDisplayMode(0, &DM) != 0){
      SDL_Log("Error : Get display size - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }
   const int height = DM.h;
   const int width = DM.w;

   window = SDL_CreateWindow("Pavé serpents", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width/1.5f, height/1.5f, SDL_WINDOW_RESIZABLE);

   if (window == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());                 
      SDL_Quit();                                     
      exit(EXIT_FAILURE);
   }

   SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
   if (renderer == NULL) {
      SDL_DestroyWindow(window);
      fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }

   int quit = 0;
   SDL_Event e;
   float t = 0;
   float dt = 0.2f;

   Object *objs = (Object*)malloc(NBSQUARES * sizeof(Object)); 
   Object_init_random(objs, width, height);

   while (!quit) {
      while (SDL_PollEvent(&e)) {
         switch (e.type) {
         case SDL_QUIT  :
            printf("on quitte\n"); 
            quit = 1;
            break;

         case SDL_KEYDOWN :
            if(e.key.keysym.sym == SDLK_q){
               quit = 1;
               printf("on quitte\n");
               break;
            }
         default:
         }
      }
      SDL_Delay(DELAY);

      Squares_rain(renderer, objs, t, height);
      t = t + dt;

      SDL_RenderPresent(renderer);
      SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
      SDL_RenderClear(renderer);  
   }

   SDL_DestroyWindow(window); 
   SDL_DestroyRenderer(renderer);  
   free(objs);   
   SDL_Quit();                     

   return 0;
}