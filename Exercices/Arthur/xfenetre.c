#include <SDL2/SDL.h>

#define PI 3.14f
#define NB_ITER 10
#define DELAY 100

void create_window(SDL_Window **window, int x, int y, int w, int h, int i){
   char output[20];
   sprintf(output, "XFenêtre_%d", i);
   *window = SDL_CreateWindow(output, x, y, w, h, SDL_WINDOW_RESIZABLE);

   if (*window == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());                 
      SDL_Quit();                                     
      exit(EXIT_FAILURE);
   }
}

int main(void) {
   SDL_Window **window = (SDL_Window**)malloc(NB_ITER * sizeof(SDL_Window*));
   SDL_DisplayMode DM;

   if (SDL_Init(SDL_INIT_VIDEO) != 0) {
      SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }

   /* Taille de l'écran */
   if (SDL_GetCurrentDisplayMode(0, &DM) != 0){
      SDL_Log("Error : Get display size - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }
   const int height = DM.h;
   const int width = DM.w;

   window[0] = SDL_CreateWindow("XFenêtre", 0, 0, width, height, SDL_WINDOW_RESIZABLE);

   if (window == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());                 
      SDL_Quit();                                     
      exit(EXIT_FAILURE);
   }
   int i = 1;

   while (i != NB_ITER) {
      SDL_Delay(DELAY);
      int x, y, h, w;
      SDL_GetWindowPosition(window[i-1], &x, &y);
      SDL_GetWindowSize(window[i-1], &w, &h);

      create_window(&window[i], x + w/4, y + h/4, w/2, h/2, i);
      // printf("%d %d %d %d %d\n", x, y, w, h, i);
      ++i;
   }                         

   SDL_DestroyWindow(window[0]);
   /* Déplacement fenetre en Cercle */
   double dp = (2*PI) / (NB_ITER-1);
   double theta = 0;
   int j = 1;
   int r = width/2;
   while(theta < (2*PI)){
      int x = (r * cos(theta)); 
      int y = (r * sin(theta));
      theta += dp;
      SDL_Delay(DELAY);
      // printf("%d %d\n", x, y);
      SDL_SetWindowPosition(window[NB_ITER-j-1], x + width/2, y + height/2);
      ++j;
   }

   for(int i=1; i<NB_ITER; ++i){
      SDL_Delay(DELAY);
      SDL_SetWindowSize(window[NB_ITER-i-1], width/10, height/10);
   }

   for(int i=1; i<NB_ITER; ++i){
      SDL_Delay(DELAY);
      SDL_DestroyWindow(window[NB_ITER-i-1]);   
   }             
                 
   SDL_Quit();
   free(window);                                

   return 0;
}