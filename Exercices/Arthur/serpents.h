#ifndef _SERPENTS_H_
#define _SERPENTS_H_

#include <time.h>

typedef struct{
	int x;
	int y;
}Point;

typedef struct{
	Point p;
	int vx;
	int vy;
}Movement;

typedef struct{
	Point init_pos;
	SDL_Color c;
	int size;
	Movement m;
}Object;

Point Point_init(int x, int y);
Movement Movement_init(int vx , int x, int y);
Object Object_init(int vx, int x, int y, int size, int r, int g, int b);
void Update_object(Object *obj, float t);
void Show_object(Object obj);
void Object_init_random(Object *objs, int width, int height);
void Squares_rain(SDL_Renderer *renderer, Object *objs, float t, int height);

#endif