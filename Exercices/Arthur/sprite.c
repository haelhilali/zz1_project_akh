#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <time.h>

#define DELAY 80
#define NB_TREE 20

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
   (void)window;
   SDL_Surface *my_image = NULL;
   SDL_Texture* my_texture = NULL;         

   my_image = IMG_Load(file_image_name);
   if (my_image == NULL){ 
      printf("Echec image : %s\n", IMG_GetError());
      // exit(EXIT_FAILURE);
   }

   my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
   SDL_FreeSurface(my_image); 
   if (my_texture == NULL){
      printf("Echec transfo texture: %s\n", IMG_GetError());
      // exit(EXIT_FAILURE);
   }
   return my_texture;
}

void show_background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer) {
   SDL_Rect 
   source = {0},           // Rectangle définissant la zone de la texture à récupérer
   window_dimensions = {0},// Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
   destination = {0};      // Rectangle définissant où la zone_source doit être déposée dans le renderer

   SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h); // Récupération des dimensions de la fenêtre
   SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);       // Récupération des dimensions de l'image

   destination = window_dimensions; // On fixe les dimensions de l'affichage à  celles de la fenêtre

   /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */
   SDL_RenderCopy(renderer, my_texture, &source, &destination);                
}

void show_sprite(SDL_Texture *bg_texture, SDL_Texture *texture, SDL_Texture *texture1, SDL_Texture *texture2, SDL_Window *window, SDL_Renderer *renderer) {
   SDL_Rect 
   source = {0}, source1 = {0}, source2 = {0},
   window_dimensions = {0},
   destination = {0}, destination1 = {0};                       

   SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h); 
   SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);  
   SDL_QueryTexture(texture1, NULL, NULL, &source1.w, &source1.h); 
   SDL_QueryTexture(texture2, NULL, NULL, &source2.w, &source2.h); 

   int nb_images = 9;
   int nb_images1 = 48; 
   int nb_images2 = 6; 
   int nb_images_animation = 1 * nb_images1;
   float zoom = 2;                             // zoom, car ces images sont un peu petites
   float zoom1 = 1;   
   float zoom2 = 1;   
   int offset_x = source.w / 9, offset_y = source.h ;       // La hauteur d'une vignette de l'image
   int offset_x1 = source1.w / 6, offset_y1 = source1.h / 8; 
   int offset_x2 = source2.w / 4, offset_y2 = source2.h / 3; 
   SDL_Rect state[nb_images]; 
   SDL_Rect state1[nb_images1]; 
   SDL_Rect state2[nb_images2]; 

    /* construction des différents rectangles autour de chacune des vignettes de la planche */
   int i = 0;                                   
   for (int y = 0; y < source.h ; y += offset_y) {
      for (int x = 0; x < source.w; x += offset_x) {
         state[i].x = x;
         state[i].y = y;
         state[i].w = offset_x;
         state[i].h = offset_y;
         ++i;
      }
   }
   destination.w = offset_x * zoom;            
   destination.h = offset_y * zoom;            
   destination.x = window_dimensions.w / 2 - destination.w/2 + 200; 
   destination.y = window_dimensions.h / 2 - destination.h/2 + 200; 

   int i1 = 0;
   for (int y = 0; y < source1.h ; y += offset_y1) {
      for (int x = 0; x < source1.w; x += offset_x1) {
         state1[i1].x = x;
         state1[i1].y = y;
         state1[i1].w = offset_x1;
         state1[i1].h = offset_y1;
         ++i1;
      }
   } 
   destination1.w = offset_x1 * zoom1;            
   destination1.h = offset_y1 * zoom1;            
   destination1.x = window_dimensions.w / 2 - destination1.w / 2 - 200; 
   destination1.y = window_dimensions.h / 2 - destination1.h / 2 + 200; 

   int i2 = 0;
   for (int y = 0; y < source2.h ; y += offset_y2) {
      for (int x = 0; x < source2.w; x += offset_x2) {
         state2[i2].x = x;
         state2[i2].y = y;
         state2[i2].w = offset_x2;
         state2[i2].h = offset_y2;
         ++i2;
      }
   } 

   SDL_Rect *destination_sprite = (SDL_Rect *)malloc(NB_TREE * sizeof(SDL_Rect));
   for(int k = 0; k<NB_TREE; ++k){
      destination_sprite[k].w = offset_x2 * zoom2;             
      destination_sprite[k].h = offset_y2 * zoom2;            
      destination_sprite[k].x = rand() % window_dimensions.w ; 
      destination_sprite[k].y = rand() % window_dimensions.h / 4;
   }

   i = 0;
   i1 = 0;
   i2 = 0;
   for (int cpt = 0; cpt < nb_images_animation ; ++cpt) {
      show_background(bg_texture, window, renderer); 
      SDL_RenderCopy(renderer, texture, &state[i], &destination);
      SDL_RenderCopy(renderer, texture1, &state1[i1], &destination1);

      for(int k = 0; k<NB_TREE; ++k){
         SDL_RenderCopy(renderer, texture2, &state2[i2], &destination_sprite[k]);
         destination_sprite[k].x = (destination_sprite[k].x + window_dimensions.w / nb_images_animation) % (window_dimensions.w);
      }

      i = (i + 1) % nb_images;                   
      i1 = (i1 + 1) % nb_images1;      
      i2 = (i2 + 1) % nb_images2;                   
      SDL_RenderPresent(renderer);              
      SDL_Delay(DELAY);                           
      SDL_RenderClear(renderer);                  
   }
   free(destination_sprite);
}

int main(void) {
   SDL_Window *window = NULL;
   SDL_DisplayMode DM;
   srand(time(NULL));

   int flags = IMG_INIT_JPG|IMG_INIT_PNG;
   int initted = IMG_Init(flags);

   if((initted&flags) != flags){
      printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
      printf("IMG_Init: %s\n", IMG_GetError());
      exit(EXIT_FAILURE);
   }

   if (SDL_Init(SDL_INIT_VIDEO) != 0) {
      SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }

   /* Taille de l'écran */
   if (SDL_GetCurrentDisplayMode(0, &DM) != 0){
      SDL_Log("Error : Get display size - %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }
   const int height = DM.h;
   const int width = DM.w;

   window = SDL_CreateWindow("Sprite", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width/1.5f, height/1.5f, SDL_WINDOW_RESIZABLE);

   if (window == NULL) {
      SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());                 
      SDL_Quit();                                     
      exit(EXIT_FAILURE);
   }

   SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
   if (renderer == NULL) {
      SDL_DestroyWindow(window);
      fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
   }

   SDL_Texture *Explosions = load_texture_from_image("./img_sprite/Explosions.png", window, renderer);
   SDL_Texture *Tree = load_texture_from_image("./img_sprite/Tree.png", window, renderer);
   SDL_Texture *Warrior_Purple = load_texture_from_image("./img_sprite/Warrior_Purple.png", window, renderer);
   SDL_Texture *Carved_9Slides = load_texture_from_image("./img_sprite/Carved_9Slides.png", window, renderer);

   int quit = 0;
   SDL_Event e;

   SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
   SDL_RenderClear(renderer);  

   while (!quit) {
      while (SDL_PollEvent(&e)) {
         switch (e.type) {
         case SDL_QUIT  :
            printf("on quitte\n"); 
            quit = 1;
            break;

         case SDL_KEYDOWN :
            if(e.key.keysym.sym == SDLK_q){
               quit = 1;
               printf("on quitte\n");
               break;
            }
         default:
         }
      }
      show_sprite(Carved_9Slides, Explosions, Warrior_Purple, Tree, window, renderer); 
   }

   SDL_DestroyWindow(window); 
   SDL_DestroyRenderer(renderer);
   SDL_DestroyTexture(Warrior_Purple);
   SDL_DestroyTexture(Tree);
   SDL_DestroyTexture(Explosions);
   SDL_DestroyTexture(Carved_9Slides);
   IMG_Quit(); 
   SDL_Quit();                     

   return 0;
}